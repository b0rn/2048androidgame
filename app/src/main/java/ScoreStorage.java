package com.example.a2048;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Date;

public class ScoreStorage extends SQLiteOpenHelper {
    private static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "2048";
    public static final String PERSON_TABLE_NAME = "scores";
    public static final String PERSON_COLUMN_ID = "_id";
    public static final String PERSON_COLUMN_NAME = "name";
    public static final String PERSON_COLUMN_SCORE  = "score";
    public static final String PERSON_COLUMN_DATE = "date";

    private SimpleDateFormat format = new SimpleDateFormat("dd/MM/YYYY");


    public ScoreStorage(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL("CREATE TABLE " + PERSON_TABLE_NAME + " (" +
                PERSON_COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                PERSON_COLUMN_NAME + " TEXT, " +
                PERSON_COLUMN_SCORE + " INTEGER, " +
                PERSON_COLUMN_DATE + " TEXT )");
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + PERSON_TABLE_NAME);
        onCreate(sqLiteDatabase);
    }


    public void saveScore(String name, int score) {
        SQLiteDatabase database = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put(this.PERSON_COLUMN_NAME, name);
        values.put(this.PERSON_COLUMN_SCORE,score);
        values.put(this.PERSON_COLUMN_DATE,format.format(new Date()));

        database.insert(this.PERSON_TABLE_NAME, null, values);
    }

    public Cursor readScores() {

        SQLiteDatabase database = getReadableDatabase();

        String selection = "SELECT * FROM "+this.PERSON_TABLE_NAME;

        Cursor cursor = database.query(
                this.PERSON_TABLE_NAME,   // The table to query
                null,                               // The columns to return
                null,                                // The columns for the WHERE clause
                null,                            // The values for the WHERE clause
                null,                                     // don't group the rows
                null,                                     // don't filter by row groups
                PERSON_COLUMN_SCORE+" DESC "
        );
        return cursor;
    }


}
