package com.example.a2048;

import android.util.Log;

import java.util.Arrays;
import java.util.Random;
import java.util.stream.IntStream;

public class GameCore {
    public static enum DIRECTION{UP,DOWN,LEFT,RIGHT};

    private int m_dimension;
    private int m_values[][];
    private int m_proportion; // proportion de 2 par rapport aux 4 (sur 10)
    private Random m_rand;
    private int m_target;
    private int m_score;

    public GameCore(int dimension,int target){
        this.m_rand = new Random();
        this.m_dimension = dimension;
        this.m_values = new int[dimension][dimension];
        this.m_target = target;
        this.m_proportion = 6;
        this.m_score = 0;
    }

    public int getDimension(){return this.m_dimension;}
    public int[][] getValues(){return this.m_values;}
    public int getScore(){return this.m_score;}

    public void reset(){
        for(int[] t : this.m_values)
            Arrays.fill(t,0);
        this.m_score = 0;
        this.addValue(this.place(this.m_dimension*this.m_dimension),this.newValue());
        this.addValue(this.place((this.m_dimension*this.m_dimension)-1),this.newValue());
    }

    private boolean addValue(int place,int value){
        int count = 0;
        for(int i = 0; i < this.m_dimension; i++){
            for(int j=0; j < this.m_dimension; j++){
                if(this.m_values[i][j] == 0) {
                    if (count == place) {
                        this.m_values[i][j] = value;
                        return true;
                    }
                    count++;
                }
            }
        }
        return false;
    }

    private int newValue(){
        if(this.m_rand.nextInt(10) < this.m_proportion)
            return 2;
        return 4;
    }

    private int place(int nbZeros){
        return this.m_rand.nextInt(nbZeros);
    }

    public boolean won(){
        for(int[] t : this.m_values){
            for(int v : t) {
                if (v == this.m_target)
                    return true;
            }
        }
        return false;
    }

    public boolean failed(){
        for(int i = 0; i < this.m_dimension; i++){
            for(int j = 0; j < this.m_dimension; j++){
                int v = this.m_values[i][j];

                if(v == 0)
                    return false;

                if(i > 0 && this.m_values[i-1][j] == v)
                    return false;
                if(i < this.m_dimension-1 && this.m_values[i+1][j] == v)
                    return false;
                if(j > 0 && this.m_values[i][j-1] == v)
                    return false;
                if(j < this.m_dimension-1 && this.m_values[i][j+1] == v)
                    return false;
            }
        }
        return true;
    }

    public void combine(DIRECTION direction){
        int count = 0;

        if(direction == DIRECTION.UP || direction == DIRECTION.DOWN){
            for(int j = 0; j < this.m_dimension; j++)
                count += this.combine(direction,0,j);
        }else{
            for(int i = 0; i < this.m_dimension; i++)
                count += this.combine(direction,i,0);
        }

        if(count > 0)
            this.addValue(this.place(count),this.newValue());
    }

    private int combine(DIRECTION direction,int i,int j){
        int iC = -1, jC = -1, lastValue = 0;

        switch(direction){
            case LEFT:
                j = this.m_dimension-1;
                break;
            case RIGHT:
                j = 0;
                break;
            case UP:
                i = this.m_dimension-1;
                break;
            case DOWN:
                i = 0;
                break;
        }

        while(j < this.m_dimension && i < this.m_dimension && i >= 0 && j >= 0){
            if(lastValue == this.m_values[i][j] && lastValue != 0){
                this.m_values[i][j] *= 2;
                this.m_score += this.m_values[i][j];
                this.m_values[iC][jC] = 0;
                lastValue = 0;
                iC = -1;
                jC = -1;
            }else if(this.m_values[i][j] != 0){
                lastValue = m_values[i][j];
                iC = i;
                jC = j;
            }
            switch(direction){
                case LEFT:j--;break;
                case RIGHT:j++;break;
                case UP:i--;break;
                case DOWN:i++;break;
            }

        }

        // trim
        switch(direction){
            // besoin d'aller à l'envers
            case LEFT:
                j = 0;
                break;
            case RIGHT:
                j = this.m_dimension-1;
                break;
            case UP:
                i = 0;
                break;
            case DOWN:
                i = this.m_dimension-1;
                break;
        }

        int zeros = 0, count = 0;

        while(j < this.m_dimension && i < this.m_dimension && i >= 0 && j >= 0){
            if(this.m_values[i][j] == 0){ zeros++;count++;}
            else if(zeros > 0){
                switch(direction)
                {
                    case LEFT:this.m_values[i][j-zeros] = this.m_values[i][j];break;
                    case RIGHT:this.m_values[i][j+zeros] = this.m_values[i][j];break;
                    case UP:this.m_values[i-zeros][j] = this.m_values[i][j];break;
                    case DOWN:this.m_values[i+zeros][j] = this.m_values[i][j];break;
                }
                this.m_values[i][j] = 0;
            }
            switch(direction){
                case LEFT:j++;break;
                case RIGHT:j--;break;
                case UP:i++;break;
                case DOWN:i--;break;
            }
        }

        return count;
    }
}
