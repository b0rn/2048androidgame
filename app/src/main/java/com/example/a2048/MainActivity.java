package com.example.a2048;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void loadGame(View v){
        Intent otherActivity = new Intent(getApplicationContext(),Game.class);
        startActivity(otherActivity);
        finish();
    }

    public void loadScores(View v){
        Intent otherActivity = new Intent(getApplicationContext(),Scores.class);
        startActivity(otherActivity);
        finish();
    }
}
