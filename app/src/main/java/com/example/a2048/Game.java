package com.example.a2048;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.ShapeDrawable;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GestureDetectorCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.TableLayout;
import android.widget.TableRow;

import java.util.Arrays;
import java.util.Random;

import com.example.a2048.GameCore;

public class Game extends AppCompatActivity{

    class MyGestureDetector extends GestureDetector.SimpleOnGestureListener {
        private static final int SWIPE_MIN_DISTANCE = 120;
        private static final int SWIPE_THRESHOLD_VELOCITY = 55;

        @Override
        public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX,
                               float velocityY) {
            try {
                // right to left swipe
                if (e1.getX() - e2.getX() > SWIPE_MIN_DISTANCE
                        && Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {
                    Game.this.swipe(GameCore.DIRECTION.LEFT);
                }
                // left to right swipe
                else if (e2.getX() - e1.getX() > SWIPE_MIN_DISTANCE
                        && Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {
                    Game.this.swipe(GameCore.DIRECTION.RIGHT);
                }
                // bottom to top
                else if(e1.getY() - e2.getY() > SWIPE_MIN_DISTANCE
                        && Math.abs(velocityY) > SWIPE_THRESHOLD_VELOCITY) {
                    Game.this.swipe(GameCore.DIRECTION.UP);
                }
                // top to bottom
                else if(e2.getY() - e1.getY() > SWIPE_MIN_DISTANCE
                        && Math.abs(velocityY) > SWIPE_THRESHOLD_VELOCITY) {
                    Game.this.swipe(GameCore.DIRECTION.DOWN);
                }
            } catch (Exception e) {

            }
            return false;
        }

        @Override
        public boolean onDoubleTap(MotionEvent event) {
            System.out.println("onDoubleTap: " + event.toString());
            return true;
        }

        @Override
        public boolean onDown(MotionEvent event) {
            System.out.println("onDown: " + event.toString());
            return true;
        }
    }

    private static int ROWS = 4;

    private MyGestureDetector gestureDetector;
    private GestureDetectorCompat mDetector;
    private TableLayout tableLayout;
    private Button mTiles[][];
    private GameCore mGameCore;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);

        this.gestureDetector = new MyGestureDetector();
        this.mDetector = new GestureDetectorCompat(this,this.gestureDetector);
        this.mDetector.setOnDoubleTapListener(this.gestureDetector);
        this.tableLayout = (TableLayout) findViewById(R.id.tableLayout);
        this.mTiles = new Button[this.ROWS][this.ROWS];
        this.mGameCore = new GameCore(this.ROWS,2048);

        this.reset(null);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event){
        this.mDetector.onTouchEvent(event);
        return super.onTouchEvent(event);
    }


    private void swipe(GameCore.DIRECTION d){
        this.mGameCore.combine(d);
        this.updateGrid();

        Intent intent = new Intent(getBaseContext(),game_ended.class);
        intent.putExtra("SCORE_RESULT",this.mGameCore.getScore());
        if(this.mGameCore.won()){
            intent.putExtra("GAME_RESULT",true);
            startActivity(intent);
        }
        else if(this.mGameCore.failed()){
            intent.putExtra("GAME_RESULT",false);
            startActivity(intent);
        }
    }

    public void reset(View v){
        this.createGrid();
        this.mGameCore.reset();
        this.updateGrid();
    }

    private void createGrid(){
        this.tableLayout.removeAllViews();

        TableRow.LayoutParams lp = new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,TableRow.LayoutParams.WRAP_CONTENT);
        lp.setMargins(10,10,10,10);

        for(int i=0;i < this.ROWS; i++){
            TableRow row = new TableRow(this);
            row.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,TableRow.LayoutParams.WRAP_CONTENT));

            for(int j = 0; j < this.ROWS; j++){
                Button b = new Button(this);
                mTiles[i][j] = b;
                b.setLayoutParams(lp);
                row.addView(b);
            }
            this.tableLayout.addView(row,lp);
        }
    }

    private void updateGrid(){
        Button scoreB = (Button) findViewById(R.id.igScore);
        scoreB.setText(String.valueOf(this.mGameCore.getScore()));

        int[][] values = this.mGameCore.getValues();
        int dim = this.mGameCore.getDimension();
        for(int i = 0; i < dim; i++){
            for(int j = 0; j < dim; j++){
                int v = values[i][j];

                int textColorId,backgroundColorId;
                switch(v){
                    case 0:
                        textColorId = getResources().getColor(R.color.text_empty);
                        backgroundColorId = getResources().getColor(R.color.cell_empty);
                        break;
                    case 2:
                        textColorId = getResources().getColor(R.color.text_2);
                        backgroundColorId = getResources().getColor(R.color.cell_2);
                        break;
                    case 4:
                        textColorId = getResources().getColor(R.color.text_4);
                        backgroundColorId = getResources().getColor(R.color.cell_4);
                        break;
                    case 8:
                        textColorId = getResources().getColor(R.color.text_8);
                        backgroundColorId = getResources().getColor(R.color.cell_8);
                        break;
                    case 16:
                        textColorId = getResources().getColor(R.color.text_16);
                        backgroundColorId = getResources().getColor(R.color.cell_16);
                        break;
                    case 32:
                        textColorId = getResources().getColor(R.color.text_32);
                        backgroundColorId = getResources().getColor(R.color.cell_32);
                        break;
                    case 64:
                        textColorId = getResources().getColor(R.color.text_64);
                        backgroundColorId = getResources().getColor(R.color.cell_64);
                        break;
                    case 128:
                        textColorId = getResources().getColor(R.color.text_128);
                        backgroundColorId = getResources().getColor(R.color.cell_128);
                        break;
                    case 256:
                        textColorId = getResources().getColor(R.color.text_256);
                        backgroundColorId = getResources().getColor(R.color.cell_256);
                        break;
                    case 512:
                        textColorId = getResources().getColor(R.color.text_512);
                        backgroundColorId = getResources().getColor(R.color.cell_512);
                        break;
                    case 1024:
                        textColorId = getResources().getColor(R.color.text_1024);
                        backgroundColorId = getResources().getColor(R.color.cell_1024);
                        break;
                    case 2048:
                        textColorId = getResources().getColor(R.color.text_2048);
                        backgroundColorId = getResources().getColor(R.color.cell_2048);
                        break;
                    default:
                        textColorId = getResources().getColor(R.color.text_default);
                        backgroundColorId = getResources().getColor(R.color.cell_default);
                }

                this.setButtonColor(this.mTiles[i][j],textColorId,backgroundColorId);
                if(v != 0)
                    this.mTiles[i][j].setText(String.valueOf(v));
                else
                    this.mTiles[i][j].setText("");
            }
        }
    }

    private void setButtonColor(Button button,int textColorId,int backgroundColorId){
        GradientDrawable rounded_button = (GradientDrawable) getResources().getDrawable(R.drawable.rounded_button).mutate();
        rounded_button.setColor(backgroundColorId);
        button.setBackground(rounded_button);
        button.setTextColor(textColorId);
    }

    public void goToEnded(View v){
        Intent intent = new Intent(getBaseContext(),game_ended.class);
        intent.putExtra("SCORE_RESULT",this.mGameCore.getScore());
        intent.putExtra("GAME_RESULT",false);
        startActivity(intent);
    }

    public void goToMenu(View v){
        Intent intent = new Intent(getBaseContext(),MainActivity.class);
        startActivity(intent);
    }

    public void goToLeaderboards(View v){
        Intent intent = new Intent(getBaseContext(),Scores.class);
        startActivity(intent);
    }
}
