package com.example.a2048;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import org.w3c.dom.Text;

import com.example.a2048.ScoreStorage;

public class game_ended extends AppCompatActivity {

    private TextView m_gameResult;
    private TextView m_scoreResult;
    private EditText m_editText;
    private ScoreStorage m_scoreStorage;

    private int m_score;
    private boolean m_won;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game_ended);

        m_scoreStorage = new ScoreStorage(this);

        m_gameResult = (TextView) findViewById(R.id.gameResult);
        m_scoreResult = (TextView) findViewById(R.id.scoreResult);
        m_editText = (EditText) findViewById(R.id.nameTextEdit);

        Bundle b = getIntent().getExtras();
        m_score = (int) b.get("SCORE_RESULT");
        m_won = (boolean) b.get("GAME_RESULT");

        m_scoreResult.setText("Score : "+String.valueOf(m_score));

        if(m_won)
            m_gameResult.setText(R.string.won);
        else
            m_gameResult.setText(getResources().getText(R.string.lost));
    }

    public void saveScore(View v){
        String name = m_editText.getText().toString();
        if(!name.isEmpty()) {
            m_scoreStorage.saveScore(name,m_score);
            Intent intent = new Intent(getBaseContext(), MainActivity.class);
            startActivity(intent);
        }
    }

    public void toScores(View v){
        Intent intent = new Intent(getBaseContext(),Scores.class);
        startActivity(intent);
    }

    public void newGame(View v){
        Intent intent = new Intent(getBaseContext(),Game.class);
        startActivity(intent);
    }
}
