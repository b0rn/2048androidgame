package com.example.a2048;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

import com.example.a2048.ScoreStorage;

public class Scores extends AppCompatActivity {

    public class Score {
        public int score;
        public String date;
        public String name;

        public Score(){
            this.score = 0;
            this.date = "None";
            this.name = "None";
        }
    }


    public class ScoreAdapter extends BaseAdapter{
        private final ArrayList mData;

        public ScoreAdapter(Cursor it){
            this.mData = new ArrayList();

            it.moveToFirst();
            while(!it.isAfterLast()){
                Score s = new Score();
                s.score = it.getInt(it.getColumnIndex(ScoreStorage.PERSON_COLUMN_SCORE));
                s.name = it.getString(it.getColumnIndex(ScoreStorage.PERSON_COLUMN_NAME));
                s.date = it.getString(it.getColumnIndex(ScoreStorage.PERSON_COLUMN_DATE));
                mData.add(s);
                it.moveToNext();
            }
        }

        @Override
        public int getCount(){
            return this.mData.size();
        }

        @Override
        public Score getItem(int position) {
            return (Score) mData.get(position);
        }

        @Override
        public long getItemId(int position) {
            // TODO implement you own logic with ID
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            final View result;

            if (convertView == null) {
                result = LayoutInflater.from(parent.getContext()).inflate(R.layout.map_adapter_item, parent, false);
            } else {
                result = convertView;
            }

            Score item = getItem(position);

            ((TextView) result.findViewById(R.id.text1Adap)).setText("Nom : "+item.name);
            ((TextView) result.findViewById(R.id.text2Adap)).setText("Score : "+item.score);
            ((TextView) result.findViewById(R.id.text3Adap)).setText("Date : "+item.date);


            return result;
        }


    }

    private ListView m_listView;
    private ScoreStorage m_scoreStorage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scores);

        m_listView = findViewById(R.id.listScores);

        this.m_scoreStorage = new ScoreStorage(this);
        ScoreAdapter adapter = new ScoreAdapter(this.m_scoreStorage.readScores());
        m_listView.setAdapter(adapter);
    }

    public void goToMenu(View v){
        Intent intent = new Intent(getBaseContext(), MainActivity.class);
        startActivity(intent);
    }
}

